;;; xdg-extended-categories
;;; classify xdg entries according to extended categories, taking into account
;;; the categories hierarchy

(defpackage "XDG-EC"
  (:use :common-lisp)
  (:export categories
		   initialize-categories
		   get-parent-chain
		   list-categories
		   store-category
		   store-under-category))

(in-package "XDG-EC")

(defstruct xdg-category
  (name "" :type string)
  (parent nil) ; parent is another xdg-category, nil for root categories
  (apps '() :type list)) ; list will contain instances or xdg desktop-entry structs

;; this is the source tree of nested categories and subcategories (category names),
;; that will be parsed into xdg-category structs
(defparameter categories-tree
  '(("AudioVideo" ("Midi" "Mixer" "Music" "Sequencer" "Tuner" "TV"
				   ("AudioVideoEditing" ("AudioEditing" "VideoEditing"))
				   "Player" "Recorder" "DiscBurning"))
	("Development" ("Building" "IDE" "GUIDesigner" "Profiling" "RevisionControl" "Translation"
					"Database" "WebDevelopment"))
	("Education" ("Art" "Construction" "Languages" "Literature" "History" "Humanities" "Sports"))
	("Game" ("ActionGame" "AdventureGame" "ArcadeGame" "Amusement" "BoardGame" "BlockGame"
			 "CardGame" "KidsGame" "LogicGame" "RolePlaying" "Shooter" "Simulation"
			 "SportsGame" "StrategyGame"))
	("Graphics" ("2DGraphics" "3DGraphics" "OCR" "Photography" "RasterGraphics" "Scanning"
				 "VectorGraphics" "Viewer"))
	("Network" ("Email" "Dialup" "InstantMessaging" "Chat" "IRCClient" "Feed" "FileTransfer"
				"HamRadio" "News" "P2P" "RemoteAccess" "Telephony" "VideoConference" "WebBrowser"))
	("Office" ("Calendar" "ContactManagement" "Dictionary" "Chart" "Finance" "Flowchart" "PDA"
			   "ProjectManagement" "Presentation" "Spreadsheet" "WordProcessor" "Publishing"))
	("Science" ("ArtificialIntelligence" "Astronomy" "Biology" "Chemistry" "ComputerScience"
				"DataVisualization" "Economy" "Electricity" "Electronics" "Engineering"
				"Geography" "Geology" "Geoscience" "ImageProcessing" "Maps" "Math" "NumericalAnalysis"
				"MedicalSoftware" "Physics" "Robotics" "ParallelComputing"))
	("Settings" ("DesktopSettings" "HardwareSettings" "Printing" "PackageManager"))
	("System" ("Core" "Documentation" "TerminalEmulator" "Filesystem" "Monitor" "Security"))
	("Utilities" ("TextTools" "TelephonyTools" "Archiving" "Compression" "FileTools" "Accessibility"
				  "Calculator" "Clock" "TextEditor"))))

;; this contains the flat hash of xdg-category structs, with hash index being the category name.
;; IOW categories-tree is flattened while its hierarchical nature is reflected into the 'parent chain
;; of the struct

;;; key is the category or sub-category name
;;; attached value is an assoc with a 'parent' key (nil for a category, parent category
;;; name for a sub-category) and an 'applications' key containing the list of applications
;;; (as found in *.desktop files) that belong under that (sub)category.

;;; *.desktop files don't differentiate between categories and sub-cats in the Categories:
;;; field ; moreover they may happen in any order.

;;; note that an app may belong to several categories, so we're using lists
;;; at every step (i.e. same app will end up stored under several subcats
;;; or several categories
(defparameter categories (make-hash-table :test 'equal))


(defun store-under-category (app-category app-item)
  "store app-item (a desktop-entry struct) under category app-category (an xdg-category), that is add it
to the app-category's APPS list."
  (if (gethash app-category categories)
	  (setf (xdg-category-apps (gethash app-category categories))
			(append (list app-item)
					(xdg-category-apps (gethash app-category categories))))))
	  ;;(format t "ignoring unknown category~%")))

(defun store-category (parent-cat child-cat)
  "if child-cat is not already a key into the categories hash, make an xdg-category struct
for it, set its parent to parent-cat (the name of its parent) and store it"
  (let ((new-cat (make-xdg-category)))
	(if (not (gethash child-cat categories))
		(progn
		  (setf (xdg-category-name new-cat) child-cat
				(xdg-category-parent new-cat) parent-cat)
		  
		  (setf (gethash child-cat categories) new-cat)))
	new-cat))

(defun parent-child (entry)
  "for a given entry from the categories-list tree, create xdg-category struct and store it,
recursively if the entry's tail is itself a list"
  (let ((parent (car entry))
		(child (cadr entry)))
	(dolist (cat child)
	  (if (listp cat)
		  (progn
			(store-category parent (car cat))
			(parent-child cat))
		  (progn
			(if (not (gethash parent categories))
				(store-category nil parent))
			(store-category parent cat))))))

(defun initialize-categories ()
  "blank out and refill the cateogories hash from the categories tree"
  (clrhash categories)
  (dolist (entry categories-tree)
	(parent-child entry)))

(defun get-parent (category)
  "return the parent (name of parent category) of the given xdg-category"
  (if (gethash category categories)
	  (xdg-category-parent (gethash category categories))))

(defun list-categories (&key (include-sub nil))
  "Show a readable layout of the categories hash. If :include-sub is true,
list all categories hierarchically, otherwise (default) only show top-level
categories"
  (let ((cats '()))
	(maphash (lambda (cat-name cat-struct)
			   (if (or include-sub
					   (and (not include-sub)
							(not (xdg-category-parent cat-struct))))
				   (setf cats (append cats (list cat-name cat-struct)))))
			 categories)
	cats))

(defun get-parent-chain (category &optional (parent-chain '()))
  "return the list of names of the parents to the given category, back to the 
corresponding root category. The root category appears last."
  (let ((parent (get-parent category)))
	(if (not parent)
		parent-chain
		(get-parent-chain parent (append parent-chain (list parent))))))
	  
