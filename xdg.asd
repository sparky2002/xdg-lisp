;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

;;;(defpackage #:xdg-asd
;;; (:use :cl :asdf))

;;;(in-package :xdg-asd)

(defsystem "xdg"
  :name "xdg"
  :version "0.0.3"
  :author "Alexis Parseghian"
  :licence "to be determined"
  :description "XDG applications handling"
  :long-description "Parse XDG application files into their proper categories"
  :serial t
  :components ((:file "xdg-extended-categories")
			   (:file "xdg")))
  
