(defpackage "XDG"
  (:use :common-lisp :xdg-ec)
  (:export all-desktop-files
		   desktop-files
		   load-desktop-entry
		   load-categorize-entries
		   print-categorized-entries
		   prop-for-lang))
(require "split-sequence")
;;;(require "xdg-extended-categories")

(in-package "XDG")

;; where to look for .desktop files									
(defparameter desktop-files-path '("/usr/local/share/applications"
								   "/usr/share/applications"))

;;; maps an *.desktop XDG entry
;;; *-lang are desktop entry keys that are mapped to a country code
(defstruct desktop-entry
  (name "" :type string)
  (generic-name "" :type string)
  (comment "" :type string)
  (name-lang (make-hash-table :test 'equal) :type hash-table)
  (generic-name-lang (make-hash-table :test 'equal) :type hash-table)
  (comment-lang (make-hash-table :test 'equal) :type hash-table)
  (categories '() :type list)
  (exec "" :type string)
  (mimetype '() :type list))

(defun find-in-hash (hash look-for)
  "given a hash and something to look-for into its value, return the
list of hash keys where a match was found"
  (let ((found-keys '()))
	(maphash (lambda (k v)
			   (if (position look-for v :test 'equal)
				   (setf found-keys (append found-keys (list k)))))
			 hash)
  found-keys))

(defun prop-for-lang (entry prop lang)
  "return the language-keyed value for the given property in the desktop-entry,
e.g Name[fr]=bla => bla"
  (cond ((equal "name" prop)
		 (gethash lang (desktop-entry-name-lang entry)))
		((equal "generic-name" prop)
		 (gethash lang (desktop-entry-generic-name-lang entry)))
		((equal "comment" prop)
		 (gethash lang (desktop-entry-comment-lang entry)))))

(defun desktop-files (path)
  "retrieve list of *.desktop files under the given directory spec"
  (let ((file-spec
		 (make-pathname :directory path
						:name :wild
						:type "desktop")))
	(directory file-spec)))

(defun all-desktop-files ()
  "retrieve list of *.desktop files under all known directories 
(from xdg:desktop-files-path)"
  (let ((full-list '()))
	(mapcar (lambda (path)
			  (setf full-list (append full-list (desktop-files path))))
			desktop-files-path)
  full-list))

(defun get-lang-prop (line)
  "returns a triplet (key lang value) for xdg entries in the form
foo[bar] = blah"
  (let* ((line-items (split-sequence:split-sequence #\= line))
		 (brackets (list (position #\[ (car line-items)) (position #\] (car line-items))))
		 (prop-name (subseq (car line-items) 0 (car brackets)))
		 (prop-lang (subseq (car line-items) (1+ (car brackets)) (cadr brackets)))
		 (prop-val (cadr line-items)))
										; (format t "key: ~A lang: ~A val: ~A~%" prop-name prop-lang prop-val)
	(list prop-name prop-lang prop-val)))

(defun set-lang-prop (entry prop-lang-list)
  "set the country-indexed hash for xdg keys that have one, based on the
prop-lang-list triplet returned by get-lang-prop"
  (let ((prop-name (car prop-lang-list))
		(prop-lang (cadr prop-lang-list))
		(prop-val (caddr prop-lang-list)))
	(cond ((string= "Name" prop-name)
		   (setf (gethash prop-lang (desktop-entry-name-lang entry)) prop-val))
		  ((string= "GenericName" prop-name)
		   (setf (gethash prop-lang (desktop-entry-generic-name-lang entry)) prop-val))
		  ((string= "Comment" prop-name)
		   (setf (gethash prop-lang (desktop-entry-comment-lang entry)) prop-val)))))

(defun fill-desktop-entry (entry stream)
  "fill the desktop-entry structure entry according to the lines
read from stream"
  (let ((line "")
		(line-items '()))
	(setf line (read-line stream nil '$eof))
	(if (not (eq line '$eof))
		(progn
										; (print line)
		  (setf line-items (split-sequence:split-sequence #\= line))
										; (print line-items)
		  (cond ((eq line '$eof) (print "done"))
				((string= "Name" (car line-items))
				 (setf (desktop-entry-name entry) (cadr line-items)))
				((string= "GenericName" (car line-items))
				 (setf (desktop-entry-generic-name entry) (cadr line-items)))
				((string= "Comment" (car line-items))
				 (setf (desktop-entry-comment entry) (cadr line-items)))
				((string= "Exec" (car line-items))
				 (setf (desktop-entry-exec entry) (cadr line-items)))
				((string= "Categories" (car line-items))
				 (setf (desktop-entry-categories entry)
					   (split-sequence:split-sequence #\; (cadr line-items) :remove-empty-subseqs t)))
				((string= "MimeType" (car line-items))
				 (setf (desktop-entry-mimetype entry)
					   (split-sequence:split-sequence #\; (cadr line-items) :remove-empty-subseqs t)))
				((position #\[ (car line-items))
				 (set-lang-prop entry (get-lang-prop line)))
				(t (format nil "Unhandled key ~A" (car line-items))))
		  (fill-desktop-entry entry stream)))))

(defun load-desktop-entry (file-spec)
  "if file-spec is a proper XDG .desktop file (with [Desktop Entry] header)
create a desktop-entry structure and return it filled ; otherwise return NIL."
  (let ((line "")
		(current-entry nil))
	(with-open-file (stream file-spec :direction :input)
	  (setf line (read-line stream nil '$eof))
	  (loop while (or (equal "" line)
					  (equal (subseq line 0 1) "#"))
		 do (setf line (read-line stream nil '$eof))) ; skip comments
	  (if (string= line "[Desktop Entry]")
		  (progn
			(setq current-entry (make-desktop-entry))
			(fill-desktop-entry current-entry stream))))
	current-entry))

(defun add-to-categories (file)
  "load an interpret a desktop entry file and add it to the (known, standard) categories
that it lists"
  (let ((entry (load-desktop-entry file))
		(remove-from-cats '()))
	;;(format t "entry: ~A ~A~%" (desktop-entry-name entry) (desktop-entry-generic-name entry))
	;;(mapcar #'add-to-category (desktop-entry-categories entry) (list entry))))
	;; HERE is where care should be taken to not store an entry under all members
	;; of its category parent chain
	(format t "Listed categories ~A~%" (desktop-entry-categories entry))
	(dolist (app-category (desktop-entry-categories entry))
	  (format t "processing ~A~%" app-category)
	  (format t "parent chain ~A~%" (xdg-ec:get-parent-chain app-category))
	  (setf remove-from-cats
			(append remove-from-cats
					(xdg-ec:get-parent-chain app-category))))
	(format t "Categories to remove: ~A~%" remove-from-cats)
	(format t "Resulting list ~A~%"
			(set-difference (desktop-entry-categories entry) remove-from-cats :test 'equal))
	(dolist (valid-category (set-difference (desktop-entry-categories entry)
											remove-from-cats
											:test 'equal))
	  (format t "storing under ~A~%" valid-category)
	  (xdg-ec:store-under-category valid-category entry))))

(defun load-categorize-entries ()
  "load all desktop entries into their struct and fill the categorized categories
in the process"
  (xdg-ec:initialize-categories)
  (mapcar #'add-to-categories (all-desktop-files)))


